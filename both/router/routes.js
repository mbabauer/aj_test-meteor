FlowRouter.route('/', {
    action: function(params, queryParams) {
        FlowLayout.render('layout1', { main: "main_page" });
    }
});

FlowRouter.route('/start', {
    action: function(params, queryParams) {
        FlowLayout.render('layout1', { main: "start" });
    }
});